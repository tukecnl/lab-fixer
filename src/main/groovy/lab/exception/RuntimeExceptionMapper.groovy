package lab.exception

import groovy.util.logging.Slf4j
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
@Slf4j
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {

    @Override
    Response toResponse(RuntimeException ex) {
        println("#######")
        println("Exception")
        println("#######")
        Response response = Response.serverError()
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(['error': ex.getMessage()])
                .build()

        response
    }
}
