package lab

import com.hubspot.dropwizard.guice.GuiceBundle
import lab.exception.RuntimeExceptionMapper
import io.dropwizard.Application
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment
import lab.config.MockConfig

class MockService extends Application<MockConfig> {

    public static void main(String[] args) throws Exception {
        new MockService().run(args)
    }

    @Override
    void initialize(Bootstrap<MockConfig> bootstrap) {
        GuiceBundle<MockConfig> guilceBundle = GuiceBundle.<MockConfig> newBuilder()
                .addModule(new MockModule())
                .enableAutoConfig('lab.resources')
                .setConfigClass(MockConfig.class)
                .build()

        bootstrap.addBundle(guilceBundle)
    }

    @Override
    void run(MockConfig configuration, Environment env) throws Exception {
        env.jersey().register(new RuntimeExceptionMapper())
    }
}
