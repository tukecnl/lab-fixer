package lab.resources

import com.codahale.metrics.annotation.Timed
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.validator.constraints.Length

import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType


@Path("/hello")
@Produces(MediaType.APPLICATION_JSON)
class HelloResource {

    @GET
    @Timed
    public Greeting hello() {
        new Greeting('helooo')
    }

    static class Greeting {

        private long id

        @Length(max = 3)
        private String content;

        Greeting(String content) {
            this.id = 10
            this.content = content
        }

        @JsonProperty
        String getId() {
            return this.id
        }

        @JsonProperty
        String getContent() {
            return this.content
        }
    }
}
